

# Spike! - Naxsi Rules Builder

Spike! is a simple webapp that generates Naxsi-Rules.
Rules are stored in a sqlite-db and might be exported
into separate ruleset-files for further processing. 

This Software is intended for people or dc-operators who
already use Naxsi and are familiar with writing Naxsi-Signatures
and was intially created to help with keeping the Doxi-Rulesets
up-to-date. 

# Installation and Requirements 

- see docs/install.md



# WARNING

> 
> Spike ist still very early alpha.
>
> NEVER run Spike! on a public facing Server; there's absolutely 
> no protection or user-login atm; exposing Spike! to the public could
> lead into damaged or deleted rules 
>
> Really
>
>


# Usage

- see docs/usage.md


# Links

- [Naxsi-Sources](https://github.com/nbs-system/naxsi)
- [Doxi-Rules](https://bitbucket.org/lazy_dogtown/doxi-rules/src)
- [Naxsi-Wiki](https://github.com/nbs-system/naxsi/wiki)
