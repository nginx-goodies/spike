#! /usr/bin/env python
#
# spike-server
# 
# this: v0.8.22 - 2014-10-10
#

import sys
import os
from time import time, strftime, localtime 
from os.path import dirname, abspath, isdir, isfile
from shutil import copy, move
from spike import create_app, seeds, spike_version, random_string
from flask import Flask 


def usage():

  print "Usage: ./server [run|init|update]\n\n"
  

def main(argv):
  
  print """

Spike v: %s
  
  """ % spike_version()
  
  if len(argv) == 2:
    if argv[1] == "run":
      run()
    elif argv[1] == "init":
      spike_init()
    elif argv[1] == "update":
      spike_update()
    else: 
      usage()
  else:
    usage()
  
def run():
  app = create_app(config_file())
  
  # some defaults
  try:
    app_port = int(app.config["APP_PORT"])
  except:
    app_port = 5555
  try:
    app_host = app.config["APP_HOST"]
  except:
    app_host = "127.0.0.1"



  from spike.model import db, Settings

  db.init_app(app)
  app.test_request_context().push()

  try:
    eo_dir = Settings.query.filter(Settings.name == 'rules_export_dir').first()
    app.config["RULES_EXPORT"] = eo_dir.value
  except:
    app.config["RULES_EXPORT"] = "exports"

  

  try:
    backup_dir = Settings.query.filter(Settings.name == 'backup_dir').first()
    app.config["BACKUP_DIR"] = backup_dir.value
  except:
    app.config["BACKUP_DIR"] = "backups"

  
  try:
    eo_offset = Settings.query.filter(Settings.name == 'rules_offset').first()
    app.config["NAXSI_RULES_OFFSET"] = eo_offset.value
  except:
    app.config["NAXSI_RULES_OFFSET"] = 20000

  try:
    sqlite_bin = Settings.query.filter(Settings.name == 'sqlite_bin').first()
    app.config["SQLITE_BIN"] = sqlite_bin.value
  except:
    app.config["SQLITE_BIN"] = "/usr/bin/sqlite3"
  if app.config["SQLITE_BIN"] == None:
    app.config["SQLITE_BIN"] = "/usr/bin/sqlite3"
    
  # some checks - TODO: should put this in run/init
  od =  app.config["RULES_EXPORT"]
  if not isdir(od):
    print """
[-] ERROR: ruleset_export_dir not found: %s
    >
    > are you sure you did run: ./spike-server init ?
    >
    > if so please create the directory manually and try to restart
    >
""" % (od)

  try:
    app.config["RULESET_HEADER"] = app.config["RULESET_HEADER"]
  except:
    app.config["RULESET_HEADER"] = """"""

  
    
  print "> running @ %s:%s" % (app_host, app_port)
  app.run(debug=True, host=app_host, port=app_port)

def spike_init():
  from flask.ext.sqlalchemy import SQLAlchemy
  it = int(time())
  print "> init Spike  "
  ts = int(time())
  ds = strftime("%F - %H:%M", localtime(time()))
  app = create_app(config_file())

  od = seeds.settings_seeds['rules_export_dir']
  if not isdir(od):
    print "  > rulesets_export_dir not found, creating: %s" % od
    for eo in ["naxsi", "ossec"]:
      print "  - %s - export" % eo
      os.makedirs("%s/%s" % (od, eo))
  bd = seeds.settings_seeds['backup_dir']
  if not isdir(bd):
    print "  > rulesets_backup_dir not found, creating: %s" % bd
    os.mkdir(bd)
  
    

  db_files = app.config["SQLALCHEMY_BINDS"]
  #print db_files
  
  for sqldb in db_files:
    p1 = "spike/%s" % db_files[sqldb].replace("sqlite:///", "")
    if os.path.isfile(p1):
      print "  > [ %s ] :: existing db found, creating backup" % sqldb
      move(p1, "%s.%s" % (p1, it ))
      print "  > copy: %s.%s" % (p1, it)
  print "  > init_creating db"
  #os.system("sqlite3 %s < data/ascii_db.sql" % db_file)

  from sqlalchemy.exc import IntegrityError
  from spike.model import db
  from spike.model import NaxsiRules, NaxsiRuleSets, ValueTemplates, Settings


  app.test_request_context().push()
  db.init_app(app)

  with app.app_context():
    db.create_all()
  print "  > filling default_vals"
  vseed = seeds.vtemplate_seeds
  for v in vseed:
    print "    > adding templates: %s" % v 
    for val in vseed[v]:
      db.session.add(ValueTemplates(v, val))
  rseed = seeds.rulesets_seeds
  for r in rseed:
    print "    > adding ruleset: %s / %s" % (r, rseed[r])
    rmks = "naxsi-ruleset for %s / auto-created %s" % (r, ds)
    db.session.add(NaxsiRuleSets(rseed[r], r, rmks, ts ))

  sseed = seeds.settings_seeds 
  for s in sseed:
    print "    > adding setting: %s" % s 
    db.session.add(Settings(s, sseed[s]))
  db.session.commit()
  
  secret_key = random_string()
  
  f = open(config_file(), "a")
  f.write("""
SECRET_KEY="%s"
  """ % secret_key)

  print " > secret-key generated"
  
  f.close()
  
  #~ try:
    #~ db.session.commit()
  #~ except IntegrityError:
    #~ print "already seeded"

  print """> OK init Spike
>
> start engines: ./server run
>

  """  

def spike_update():
  from flask.ext.sqlalchemy import SQLAlchemy
  it = int(time())
  print "> update Spike  "
  print "> getting latest updates from repo"
  os.system("git pull 2>&1")
  ts = int(time())
  ds = strftime("%F - %H:%M", localtime(time()))
  app = create_app(config_file())

  from sqlalchemy.exc import IntegrityError
  from spike.model import db
  from spike.model import NaxsiRules, NaxsiRuleSets, ValueTemplates, Settings


  app.test_request_context().push()
  db.init_app(app)


  sseed = seeds.settings_seeds 
  for s in sseed:
    ks = check_constraint("settings", s)
    if not ks:
      print "    > adding setting: %s" % s 
      db.session.add(Settings(s, sseed[s]))
    else:
      print "    > setting known : %s" % s 
  db.session.commit()  

  
  
  #~ try:
    #~ db.session.commit()
  #~ except IntegrityError:
    #~ print "already seeded"


  print """> Update OK
>
> start engines: ./server run
>

  """  



def config_file():
  return os.path.join(dirname(abspath(__name__)), 'config.cfg')
  
if __name__ == "__main__":
  main(sys.argv)
